scrsel
======

### Scenario
Using GNU Screen to manage your sessions ?  
This utility offers a menu to choice between the existing sessions ; it lists all your existing screen sessions and allows you to pick one up.
### Example
```
vagrant@sophia:~$ ruby etc/scrsel
     0 exit
     1 ascp-run
     2 ascp-vim
     3 central-curl
     4 central-node
     5 central-run
     6 central-vim
     7 central2-run
     8 central2-vim
     9 gen4-run
    10 gen4-vim
    11 naw-run
    12 nax-run
    13 nax-vim
    14 qalib
    15 support-lib
please choose one of:
```
### Usage
scrsel [regex]

It can be invoked from your .profile - or equivalent.

### Goal/constraint
For now the objective is to have only one file, and no dependency. You can simply download the main file - scrsel - into a directory in your PATH.

### License
MIT
